<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function list()
    {
        return response(User::all(), 200);/*->withHeaders([
            'Content-Type' => 'application/json', 'Access-Control-Allow-Origin'=>'*', ]);*/
    }

    public function store(Request $request)
    {
        // return back();
        /*return response($request, 200)->withHeaders([
            'Content-Type' => 'application/json', 'Access-Control-Allow-Origin'=>'*', ]);*/
        $user = new User();

        $user->name = $request->get('name');
        $user->birthday = $request->get('birthday');
        $user->division = $request->get('division');
        $user->location = $request->get('location');
        $user->email = Str::random(10).'@gmail.com';
        $user->password = Str::random(16);
        $user->save();

        return response(['status'=>'success', 'id'=>$user->id], 200);
    }

    public function divisions()
    {
        return response(['PHP', 'JS', 'Java', '.Net', 'Go', 'Python'], 200)/*->withHeaders([
            'Content-Type' => 'application/json', 'Access-Control-Allow-Origin'=>'*', ])*/;
    }
}
