import { Injectable } from "@angular/core";
import { User } from "./User";
import { Observable, from } from "rxjs";
@Injectable()
export class StaticDataSource {
  private users: User[] = [
    new User(1, "User 1", "22-08-1976", "Minsk", 'PHP'),
    new User(2, "User 2", "22-08-1976", "Brest", 'PHP'),
    new User(3, "User 3", "22-08-1976", "Minsk", 'PHP'),
    new User(4, "User 4", "22-08-1976", "Brest", 'PHP'),
    new User(5, "User 5", "22-08-1976", "Minsk", 'PHP'),
    new User(6, "User 6", "22-08-1976", "Brest", 'PHP'),
    new User(7, "User 7", "22-08-1976", "Gomel", 'PHP')
  ];

  private divisions: [] = [];

  getUsers(): Observable<User[]> {
    return from([this.users]);
  }

  getDivisions(): Observable<[]> {
    return from ([this.divisions]);
  }
}
