import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';
import { UserRepository } from "./user.repository";
import { AppComponent } from './app.component';
import {UserRestService} from "./user.rest.service";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    UserRepository, UserRestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
