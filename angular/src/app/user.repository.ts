import { Injectable } from "@angular/core";
import { User } from "./User";
import {UserRestService} from "./user.rest.service";

@Injectable()

export class UserRepository {
  private users: User[] = [];
  private divisions;
  constructor(private dataSource: UserRestService) {
    dataSource.getUsers().subscribe(data => {
      this.users = data;
    });
    dataSource.getDivisions().subscribe(data => {
      this.divisions = data;
    });
  }
  getUsers(): User[] {
    return this.users;
  }
  addUser(user: User) {
    this.dataSource.addUser(user).subscribe(() => this.users.push(user));
  }

  getDivisions(): [] {
    return this.divisions;
  }
  getUser(id: number): User {
    return this.users.find(u => u.id === id);
  }
}
