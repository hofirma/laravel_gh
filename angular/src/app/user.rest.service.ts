import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { User } from "./User";

const PROTOCOL = "http";
const PORT = 8072;
const HOST = 'api.la.godeltech.com';
@Injectable()
export class UserRestService {
  baseUrl: string;
  constructor(private http: HttpClient) {
    this.baseUrl = `${PROTOCOL}://${HOST}:${PORT}/api/`;
  }
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + "users");
  }

  addUser(user: User): Observable<User> {
    return this.http.post<User>(this.baseUrl + "user", {...user});
  }

  getDivisions(): Observable<[]> {
    return this.http.get<[]>(this.baseUrl + "divisions");
  }
}
