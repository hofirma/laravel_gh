import { Component } from '@angular/core';
import { User } from "./User";
import { UserRepository } from "./user.repository";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'angular';
  constructor(private repository: UserRepository) { }

  getUsers(): User[] {
     return this.repository.getUsers();
  }

  getDivisions() {
    return this.repository.getDivisions();
  }

  addUser(name, division) {
    if (name !== '') {
      this.repository.addUser(new User(null, name, '1993-02-16', 'Minsk', division));
    }
  }
}
