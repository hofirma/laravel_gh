export class User {
  id;
  name;
  birthday;
  location;
  division;
  status;
  constructor(id, name, birthday, location, division) {
    this.id = id;
    this.name = name;
    this.birthday = birthday;
    this.location = location;
    this.division = division;
    this.status = false;
  }
}
