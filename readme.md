###### Angular + LARAVEL (6+6)
```
npm install -g @angular/cli

cd angular

npm ci

ng build --prod

cd ..

docker-compose up -d

docker exec -it laravel-app bash

php artisan migrate
php artisan db:seed

exit 
```
enjoy!
